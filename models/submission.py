from pydantic import BaseModel

from models.common import UserLogin
from models.problem import ProblemId

SubmissionId = int


class Submission(BaseModel):
    id: SubmissionId
    problem_id: ProblemId
    owner: UserLogin


class AddSubmissionRequest(BaseModel):
    problem_id: ProblemId
    owner: UserLogin
