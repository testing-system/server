from pydantic import BaseModel

from models.common import UserLogin

ProblemId = int


class Problem(BaseModel):
    id: ProblemId
    owner: UserLogin


class AddProblemRequest(BaseModel):
    owner: UserLogin
