from typing import List

from db_connector.abstract import AbstractDbConnector
from models.common import UserLogin
from models.problem import Problem, ProblemId
from models.submission import Submission, SubmissionId


class FakeDbConnector(AbstractDbConnector):
    def __init__(self):
        super().__init__()
        self.problems: List[Problem] = []
        self.submissions: List[Submission] = []

    async def init(self):
        pass

    async def clear(self):
        self.problems = []
        self.submissions = []

    async def add_problem(self, owner: UserLogin) -> ProblemId:
        new_id = len(self.problems)
        self.problems.append(Problem(id=new_id, owner=owner))
        return new_id

    async def add_submission(self, owner: UserLogin,
                             problem_id: ProblemId) -> SubmissionId:
        new_id = len(self.submissions)
        self.submissions.append(Submission(id=new_id, problem_id=problem_id,
                                           owner=owner))
        return new_id

    async def get_problems(self, owner: UserLogin) -> List[Problem]:
        return [
            problem
            for problem in self.problems
            if problem.owner == owner
        ]

    async def get_submissions(self, owner: UserLogin) -> List[Submission]:
        return [
            submission
            for submission in self.submissions
            if submission.owner == owner
        ]
