from typing import List

from models.common import UserLogin
from models.problem import Problem, ProblemId
from models.submission import Submission, SubmissionId


class AbstractDbConnector:
    async def init(self):
        raise NotImplementedError

    async def add_problem(self, owner: UserLogin) -> ProblemId:
        raise NotImplementedError

    async def add_submission(self, owner: UserLogin,
                             problem_id: ProblemId) -> SubmissionId:
        raise NotImplementedError

    async def get_problems(self, owner: UserLogin) -> List[Problem]:
        raise NotImplementedError

    async def get_submissions(self, owner: UserLogin) -> List[Submission]:
        raise NotImplementedError

    async def clear(self):
        raise NotImplementedError
