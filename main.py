import os

from fastapi import Body, FastAPI

from db_connector import AbstractDbConnector, FakeDbConnector
from models.common import UserLogin
from models.problem import ProblemId
from mq_connector import AbstractMqConnector, FakeMqConnector, ZmqConnector

db: AbstractDbConnector
mq: AbstractMqConnector

app = FastAPI()


@app.on_event("startup")
async def init():
    global db
    global mq

    db = FakeDbConnector()
    await db.init()

    if os.environ.get("USE_ZMQ", "false") == "true":
        mq = ZmqConnector(os.environ["ZMQ_HOST"])
    else:
        mq = FakeMqConnector()
    await mq.init()


@app.get("/problems")
async def get_problems(owner: UserLogin):
    return await db.get_problems(owner)


@app.get("/submissions")
async def get_submissions(owner: UserLogin):
    return await db.get_submissions(owner)


@app.post("/problems/add")
async def add_problem(owner: UserLogin = Body(..., embed=True)):
    return await db.add_problem(owner)


@app.post("/submissions/add")
async def add_submission(owner: UserLogin = Body(..., embed=True),
                         problem_id: ProblemId = Body(..., embed=True)):
    return await db.add_submission(owner, problem_id)


@app.get("/problems/clear")
async def clear_problems():
    await db.clear()
    return "OK"
