FROM python:3.7

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY db_connector .
COPY models .
COPY mq_connector .
COPY main.py .

CMD ["uvicorn", "main:app"]