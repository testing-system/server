import logging

from mq_connector.abstract import AbstractMqConnector


class FakeMqConnector(AbstractMqConnector):
    async def init(self):
        pass

    async def send(self, message: str):
        logging.debug("[%s] %s", self.send, message)
