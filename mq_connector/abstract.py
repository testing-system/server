class AbstractMqConnector:
    async def init(self):
        raise NotImplementedError

    async def send(self, message: str):
        raise NotImplementedError
