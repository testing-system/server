import zmq

from mq_connector.abstract import AbstractMqConnector


class ZmqConnector(AbstractMqConnector):
    context: zmq.Context
    socket: zmq.Socket

    def __init__(self, host: str):
        super().__init__()
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.PUSH)
        self.socket.bind(host)

    async def init(self):
        pass

    async def send(self, message: str):
        self.socket.send_string(message)
